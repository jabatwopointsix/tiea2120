// data-muuttuja sisältää kaiken tarvittavan ja on rakenteeltaan lähes samankaltainen kuin viikkotehtävässä 2
// Rastileimaukset on siirretty tupa-rakenteesta suoraan jokaisen joukkueen yhteyteen
// Rakenteeseen on myös lisätty uusia tietoja
// voit tutkia tarkemmin käsiteltävää tietorakennetta konsolin kautta 
// tai json-editorin kautta osoitteessa http://jsoneditoronline.org/
// Jos käytät json-editoria niin avaa data osoitteesta:
// http://appro.mit.jyu.fi/tiea2120/vt/vt3/data.json

"use strict";

console.log(data);


/*###########################################
BUILDERIT
############################################*/ 

/*
Hakee ensin tietorakenteesta tiedot eri leimaustavoista ja rakentaa
sen perusteella formiin leimaustavat
*/
function buildleimaustapaform() {
	// luodaan elementit
	const leimaustavatLista = document.getElementById("liLeimaustavat");
	const dataleimaustavat = getLeimaustavat();
	const lh = document.createElement("lh")
	lh.textContent = "Leimaustavat ";
	leimaustavatLista.appendChild(lh)
	for (let i in dataleimaustavat) {
		// aloitetaan loopissa lisäämään elementtejä
		const li = document.createElement("li")
		li.classList.add("floatright");
		leimaustavatLista.appendChild(li)
		const label = document.createElement("label");
		label.setAttribute("for", "leimaus_"+ dataleimaustavat[i].toLowerCase());
		li.appendChild(label);
		label.textContent = dataleimaustavat[i];
		const input = document.createElement("input");
		input.classList.add("cleimaus");
		input.type = "checkbox";
		input.id = "leimaus_"+ dataleimaustavat[i].toLowerCase();
		input.value = dataleimaustavat[i].toLowerCase();
		label.appendChild(input);
	}
}

/*
Hakee ensin sarjat tietorakenteesta ja rakentaa
sen perustella formiin sarjat
*/
function buildsarjaform() {
	const sarjat = document.getElementById("liSarja");
	const sarjojenNimet = getSarjat();
	const lh = document.createElement("lh")
	lh.textContent = "Sarjat ";
	sarjat.appendChild(lh)
	sarjojenNimet.sort();
	for (let i in sarjojenNimet) {
		const li = document.createElement("li")
		sarjat.appendChild(li)
		const label = document.createElement("label");
		label.textContent = sarjojenNimet[i];
		const input = document.createElement("input");
		input.type = "radio";
		input.classList.add("csarja");
		input.name = "radiosarja";
		input.value = sarjojenNimet[i].toLowerCase();
		if(i == 0) { 
			input.checked = true 
		}
		li.appendChild(label);
		label.appendChild(input);
	}
}

/*
Hakee jäsenet ja tekee niistä listauksen sivun loppuun
*/
function buildTeamList() {
	// haetaan elementti ja tyhjennetään siitä sisältö
	const outterUl = document.getElementById("ulTeamList");
	emptyElement(outterUl);
	// luodaan joukkueille oma lista
	const joukkueet = data.joukkueet;
	// järjestetään ensin joukkueet oikeaan järjestykseen
	joukkueet.sort(orderNameAsc)
	// loopataan järjestetyt joukkueet läpi
	for(let i in joukkueet) {
		// luodaan elementit
		const ul = document.createElement("li");
		outterUl.appendChild(ul);
		const innerUl = document.createElement("ul");
		outterUl.appendChild(innerUl);
		// asetetaan elementille tekstiarvo
		ul.textContent = joukkueet[i].nimi;
		// luodaan jäsenille oma lista
		const jasenet = joukkueet[i].jasenet;
		// laitetaan joukkueen jäsenet aakkosjärjestykseen
		jasenet.sort();
		// lisätään loopissa joukkueen jäsenet elementtiin
		for(let j in joukkueet[i].jasenet) {
			// luodaan elementit
			const innerul = document.createElement("ul");
			const innerli = document.createElement("li");
			// asetaan elementille tekstiarvo
			innerli.textContent = jasenet[j];
			// lisätään elementit
			ul.appendChild(innerul)
			innerul.appendChild(innerli)
		}
	}
}

/*
	rakentaa jäsen fieldit
*/
function buildJasenFields() {
	//tehdään viisi fieldiä
	for (var i = 0; i < 5; i++) {
		if(i < 2) {
			//vaaditut kentät
			addJasenField(true);
		} else {
			// vapaaehtoiset kentät
			addJasenField(false);
		}
		
	}
}

/*
Lisätään kentät jäsenille
*/
function addJasenField(required) {
	// luodaan sekä haetaan elemntit
	const montako = document.querySelectorAll(".memberinput").length;
	const ul = document.getElementById("liJasenet");
	const li = document.createElement("li")
	const label = document.createElement("label");
	const input = document.createElement("input");
	// asetetaan luokka
	li.classList.add("memberinput");
	ul.appendChild(li)
	label.setAttribute("for", "memberinput" + (montako + 1));
	li.appendChild(label);
	label.textContent = "Jäsen " + (montako + 1);
	input.id = "memberinput" + (montako + 1);
	input.type = "text";
	input.classList.add("memberinputtext");
	// mikäli kyseessä on vaadittu kenttä asetaan se todeksi
	if(required) {
		input.required = true;
	}
	label.appendChild(input);
}


/*###########################################
EVENTHANDELERIT
############################################*/ 

function addEventHandelers() {

	// joukkueen tallennus nappi eventhandeler
	const joukkueenTietoButton = document.getElementById("joukkueenTietoButton");
	joukkueenTietoButton.addEventListener("click", tallennaJoukkueenTiedotButton);
		
	// is the actual event listener for the button
	function tallennaJoukkueenTiedotButton(e) {
		// this is here to prevent reload of the page
		e.preventDefault();
		// lisätään joukkue
		addTeam();
	}

	const teamnameInput = document.getElementById("teamnameInput");
	teamnameInput.oninput = function () {
		
		const input = teamnameInput.value;
		// tarkistetaan, että nimi on ainakin 2 merkkiä
		if(input.length < 2) {
			teamnameInput.setCustomValidity("Nimen on oltava yli 2 merkkiä pitkä");;
		} else {
			teamnameInput.setCustomValidity("");
		}

		// tarkistetaan onko saman nimistä joukkuetta
		if(input.length >= 2 && doesTeamExist(input)) {
			teamnameInput.setCustomValidity("Saman niminen löytyy jo");
		} else {
			teamnameInput.setCustomValidity("");
		}
		teamnameInput.reportValidity();
	}


}

/*###########################################
Asioiden lisäämiseen tarvittavat funktiot
############################################*/ 

//
function addTeam() {
	// luodaan taulukko johon kerätään infot
	var teaminfo = [];
	// haetaan nimi kenttä
	const teamname = document.getElementById("teamnameInput").value;
	// pusketaan infot taulukkoon omilla funktioilla
	teaminfo.push({
		id: getNewTeamId(),
		jasenet: getTeamMembers(),
		leimaustapa: getLeimaustapa(),
		luontiaika: getLuontiaika(),
		matka: 0,
		nimi: teamname,
		pisteet: 0,
		rastit: [],
		sarja: getSarjaId(),
		seura: null
	});
	// joukkueen lisäyksestä ilmoitetaan
	if(isTeamInfoValid(teaminfo[0])) {
		// varsinainen lisäys tapahtuu tässä
		data.joukkueet.push(teaminfo[0]);
		document.getElementById("joukkueLisatty").textContent = "Joukkue " + teamname + " lisättiin.";
	} else {
		// jos ei onnistunut pistetään elementti piiloon
		document.getElementById("joukkueLisatty").hidden = "hidden";
	}

	// rakennetaan listaus joukkueista, jolloin päivittyy myös lisätty joukkue
	buildTeamList();

	// validoidaan infot
	function isTeamInfoValid(teaminfo) {
		if(teaminfo.nimi.length < 2) {
			return false;
		} 
		if(doesTeamExist(teaminfo.nimi)) {
			return false;
		}
		if(teaminfo.leimaustapa.length == 0) {
			document.getElementById("leimaustapaValitseAinakinYksi").hidden = "";
			return false;
		} else {
			document.getElementById("leimaustapaValitseAinakinYksi").hidden = "hidden";
		}
		// kaikki kunnossa jos päästiin tänne asti
		return true;
	}

	// haetaan jäsenet
	function getTeamMembers() {
		const jasenet = [];
		const jasenFields = document.querySelectorAll(".memberinputtext");
		console.log(jasenFields)
		for(var i = 0; i < jasenFields.length -1; i++) {
			console.log(i)
			console.log(jasenFields[i].value)
			var jasen = jasenFields[i].value;
			if(jasenFields[i].value.length > 0) {
				jasenet.push(jasen.trim())
			}
		}
		//nämä lisättiin tasolla 1
		//jasenet.push("Foo Bar");
		//jasenet.push("Bar Foo");
		return jasenet
	}

	// haetaan leimaustavat
	function getLeimaustapa() {
		const leimaustavat = document.querySelectorAll(".cleimaus");
		const teamLeimaustavat = [];
		for (let i in leimaustavat) {
			if(leimaustavat[i].checked) {
				teamLeimaustavat.push(leimaustavat[i].value.toUpperCase());
			}
		}
		return teamLeimaustavat;
	}

	function getLuontiaika() {
		// gets the time right now
		let luontiAika = new Date("1.1.2017 00:00");
		// formats the time to form EX: "2017-03-18 16:04:27"
		let strluontiAika = luontiAika.getFullYear() + "-" + insertLeadingZero(luontiAika.getMonth()+1) + "-" + insertLeadingZero(luontiAika.getDate()) + " " + insertLeadingZero(luontiAika.getHours()) + ":" + insertLeadingZero(luontiAika.getMinutes()) + ":" + insertLeadingZero(luontiAika.getSeconds() + "." + luontiAika.getMilliseconds());
		return strluontiAika;
	}
	// haetaan sarja id
	function getSarjaId() {
		const sarjat = document.querySelectorAll(".csarja");
		var sarja = "";
		for (let i in sarjat) {
			if(sarjat[i].checked) {
				sarja = sarjat[i].value;
			}
		}
		for (let i in data.kisat[0].sarjat) {
			//console.log(data.kisat[0].sarjat)
			if(sarja == data.kisat[0].sarjat[i].nimi) {
				return data.kisat[0].sarjat[i].id;
			}
		}
	}
}



/*###########################################
Functiot joilla voidaan hakea asioita taulukoissa
############################################*/ 

/*
generoidaan uusi sarja id
*/
function getNewTeamId() {
	while(true) {
		const joukkueet = data.joukkueet;
		const idt = [];
		// kerätään kaikkien joukkueiden idt
		for (let i in joukkueet) {
			const joukkue = joukkueet[i];
			idt.push(joukkue.id);
		}
		const id = Math.floor(Math.random() * 7000000000000000) + 4000000000000000; 
		// testataan löytyykö generoitu id, jos ei niin palautetaan takaisin generoitu id
		// jos löytyi alkaa prosessi alusta
		if (idt.indexOf(id) === -1) {
			return id;
		}
	}
}

/*
Hakee leimaustavat tietorakenteesta
palauttaa taulukon esim --> ["GPS", "NFC", "QR", "Lomake"]
*/
function getLeimaustavat() {
	var leimaustapojenNimet = [];
	const leimaustavat = data.kisat[0].leimaustapa;
	for (var i = 0; i < leimaustavat.length; i++) {
		leimaustapojenNimet.push(leimaustavat[i]);
	}
	return leimaustapojenNimet;
}


/*
Hakee sarjojen nimet tietorakenteesta
palauttaa taulukon esim --> ["4h", "2h", "8h"]
*/
function getSarjat() {
	var sarjojenNimet = [];
	const sarjat = data.kisat[0].sarjat;
	for (var i = 0; i < sarjat.length; i++) {
		const sarja = sarjat[i];
		sarjojenNimet.push(sarja.nimi)
	}
	return sarjojenNimet;
}

/*
Lukee tietorakenteen ja palauttaa kaikkien joukkueiden nimet
Esim: ["Dynamic Duo ", "Kotilot ", "Kahden joukkue", "Kaakelin putsaajat ", ...
*/
function getTeamNames() {
	const joukkueet = data.joukkueet;
	const nimet = [];
	for (let i in joukkueet) {
		const joukkue = joukkueet[i];
		nimet.push(joukkue.nimi);
	}
	return nimet;
}

/*
testaa löytyykö joukkue taulukosta
*/
function doesTeamExist(teamname) {
	const teamsThatExist = getTeamNames();
	for (let i in teamsThatExist) {
		if(teamsThatExist[i].toLowerCase().trim() == teamname.toLowerCase().trim()) {
			return true;
		}
	}
	return false;
}

/*###########################################
Utility function
############################################*/ 

// if the number is less than 0 it adds a leading zero. EX: 1 --> 01
function insertLeadingZero(number) {
	// if number is less than ten a leading zero is added
	// ex 8 --> 08
	if(number < 10) {
		// casted the integer to string
		var numberStr = number.toString();
		// 8 --> 08
		numberStr = "0" + numberStr;
		return numberStr;
	}
	//if there was nothing to do the actual number is returned as a string
	return number.toString();
}

// tyhjentää elementin sisällön
function emptyElement(element) {
	while(element.firstChild){
    	element.removeChild(element.firstChild);
	}
}

/*###########################################
Sortterit
############################################*/ 

// team name order functions asc
function orderNameAsc(a,b) {
	const nameA = a.nimi.toLowerCase();
	const nameB = b.nimi.toLowerCase();
	let comparison = 0;
	if(nameA > nameB) {
		comparison = 1;
	} else if(nameA < nameB) {
		comparison = -1; 
	}
	return comparison;
}

/*###########################################
This is where actually stuff gets executed
############################################*/ 

window.onload = function () {
	buildleimaustapaform();
	buildsarjaform();
	buildTeamList();
	buildJasenFields();
	addEventHandelers();
	
}

// data-muuttuja on sama kuin viikkotehtävässä 1.
// http://appro.mit.jyu.fi/tiea2120/vt/vt2/

"use strict";

console.log(data);

/*###########################################
Author: Janne Siltainsuu
janne.siltainsuu@gmail.com
############################################*/ 

var currentorder = "";


// prints out the information required in taso five 
// EX: Dynamic Duo, 206 p, 43 km, 07:49:45
function getCompetitionInformationByTeam() {
	var teaminformation = [];
	for (let i in data.sarjat) {
		// starts another loop that goes trough the teams in the sarja
		for (let j in data.sarjat[i].joukkueet) {
			// assigns the team as a variable, so the code will be more readable
			var joukkue = data.sarjat[i].joukkueet[j];	
			// pushes the information that is gathered to the data structure
			teaminformation.push({
				id: joukkue.id,
				// stores the team name in the key name
			    name:   joukkue.nimi,
			    // stores the sarja of team
			    sarja: 	data.sarjat[i].nimi,
			    // gets the members of the team
			    members: joukkue.jasenet,
			    // stores the score of the team in the value score
			    score: 	getTeamScoreById(joukkue.id),
			    // stores the distance traveled by the team
			    distance: Math.round(getDistanceTraveledByTeam(joukkue.id)),
			    //stores the time that the team used
			    time: getTimeUsedByTeam(joukkue.id),
			    // gets rastit visited by team
			    rastit: getRastitVisitedByTeam(joukkue.id) 
			});
		}
	}
	// console.log(teaminformation)
	// returns the information
	return teaminformation;
}

// gets a teams score by using the teams id number
// this function was created inorder to make the problem 
// in pieces that are not very big, so that it is possible to
// understand the code better
function getTeamScoreById(id) {
	// introduces the variable where the teams score is saved
	// default value is 0 because there might be a situation where the team has no score
	var teamScore = 0;
	// introduces a list of rasti which have been put in to the record, to make sure not a single rasti is calculeted twice
	var rastitInRecord = []
	// starts a loop that goes trough all the tupa markings
	for (let i in data.tupa) {
		// gets the tupa that is being handeled at this time of the loop
		var tupa = data.tupa[i];
		// gets the id of this praticular tupa marking
		var tupaId = tupa.joukkue;
		// if this tupaId matches the id of the team we are examining we start to calculate scores
		if(tupaId == id) {
			// introduces a variable rasti that is the praticular rasti we are looking for
			var rasti = tupa.rasti;
			// lets look trough the custom function has this rasti been already inserted to the scores
			// NOTE that the isValueInArray needs to be false inorder for the score to be added
			
			if(!isValueInArray(rasti, rastitInRecord)) {
				// if we are here it means that it has not been inserted in to the scoreboard
				// score is added to the teamscore variable by using a custom function that returns 
				// integer if this rasti gives out scores.
				// if this rasti does not give out any score, then the function gives back a 0 that is a neutral value
				teamScore += getRastiScoreById(rasti);
				// we are going to push the rasti to the array that holds the rastit that have already been calculated
				rastitInRecord.push(rasti);
			} 
		}	
	}	
	// returns the integer that holds the teams score that is >= 0
	return teamScore;
}

// returns distance traveled by team by the id of the team
function getDistanceTraveledByTeam(teamId) {
	// initializes a array where rastit will be stored
	const rastit = getRastitVisitedByTeam(teamId);
	// initializes a variable for the distance to be stored in
	var distance = 0;
	// loops trough the rastit that we found earlier
	for (var i = 0; i < rastit.length-1; i++) {
		// rasti 1 that is fetched
		let rasti1 = rastit[i];
		// rasti 2 that is fetched
		let rasti2 = rastit[i + 1];
		// testing that all the variables are set, if they are not set then distance is not calculated
		if((rasti1.lat > 0) && (rasti1.lon > 0) && (rasti2.lat > 0) && (rasti2.lon > 0)) {
			distance += getDistanceFromLatLonInKm(rasti1.lat, rasti1.lon, rasti2.lat, rasti2.lon);
		}
	}
	// returns the distance traveled by a spesific team
	return distance;
}

// returns all the rastit visited by the team)
function getRastitVisitedByTeam(teamId) {
	// this is where the rastit will be stored
	var rastit = [];
	// loops trough all the tupa markings
	for(let i in data.tupa) {
		// initialized a variable to keep the code more readable
		var tupa = data.tupa[i];
		// tests if this tupa marking belongs to the tam that we are examining
		if(tupa.joukkue == teamId) {
			// gets the rastiId of the tupa mrking
			const rid = tupa.rasti;
			// gets the rasti by using a custom function
			var rasti = getRastiById(rid);
			// time of rasti visit for later use
			var time = tupa.aika;
			// if the rasti was not found the custom function returns false, so this will not be executed then
			var rIndex = i;
			if(rasti) {
				// time added to the data
				rasti["index"] = rIndex;				
				rasti["time"] = time;
				rastit.push(rasti)
			}
		}	
	}
	return rastit;
}

// gets rasti if it mathes rid provided in the parameters
function getRastiById(rid) {
	// starts a loop that goes trough all the rastit in the data structure
	for (let i in data.rastit) {
		// introduces a variable that holds the praticular rasti that is being handeled to make the code more readable
		var rasti = data.rastit[i];
		// tests if this praticular rasti id is the same that we are examining
		if(rasti.id == rid) {
			// return rasti
			return rasti;
		}	
	}
	// if rasti was not found returns false
	return false;
}

// gets the time that it took for the team to go trough the competition
function getTimeUsedByTeam(teamId) {
	// gets the rastit visited by the team from a custom funtion
	var rastit = getRastitVisitedByTeam(teamId);
	// if the team has not went to any rastit, then there is no time to record so there time will be marked as zero
	if(rastit.length == 0) {
		// returns zero
		return "00:00:00";
	}
	// starts to get the first rasti as a date object
	var firstRastiTime = new Date(rastit[0]["time"]);
	// gets the last rasti as a date object for starters
	var lastRastiTime = new Date(rastit[rastit.length-1]["time"]);
	// starts a loop that checks if the first and last rasti is really first and last
	for(let i in rastit) {
		// gets the current rasti that is being examined as a date object
		var rastiTime = new Date(rastit[i]["time"]);
		// checks if it is visited before the actual last rasti
		// this actually happens at least 17 times during run time
		if(rastiTime.getTime() < lastRastiTime.getTime()) {
			// because that happens we change the last rasti time
			lastRastiTime = new Date(rastit[i]["time"]);
		}
		// checks if the first rasti is really the first visited
		if(rastiTime.getTime() > firstRastiTime.getTime()) {
			// because this also happens really often the time of first rasti is changed
			firstRastiTime = new Date(rastit[i]["time"]);
		}
	}
	// calculates the difference between start and finnish times
	var timeUsed = new Date(firstRastiTime - lastRastiTime);
	// formes a string out of the found results and also adds leading zero if the time looks funky, ex: 7:1:8 --> 07:01:08
	var timeUsedStr = (insertLeadingZero(timeUsed.getUTCHours()) + ":" + insertLeadingZero(timeUsed.getMinutes()) + ":" +  insertLeadingZero(timeUsed.getSeconds()));
	// returns the string that was formed
	return timeUsedStr;
}

// this get a rasti-information by examining the rasti id "rid"
function getRastiScoreById(rid) {
	// starts a loop that goes trough all the rastit in the data structure
	for (let i in data.rastit) {
		// introduces a variable that holds the praticular rasti that is being handeled to make the code more readable
		var rasti = data.rastit[i]
		// tests if this praticular rasti id is the same that we are examining
		if(rasti.id == rid) {
			// uses the custom function to get the rasti score from this praticular rasti
			return getRastiScore(rasti.koodi);
		}	
	}
	// there was nothing to return so we are going to return 0 that is a neutral value in this context
	return 0;
}

// gets the rasti score from the rastikoodi which is sent as a parameter
function getRastiScore(koodi) {
	// if code for some reason is blank then a zero will be returned
	// this is here to prevent errors
	if(koodi.length == 0) {
		return 0;
	}
	// introduces a variable that will hold the first digit of the rasti koodi
	var firstDigit = koodi.substring(0,1);
	// if the number is actually a whole number then the integer is parsed and returned as an interer for point calculation
	if(isWholeNumber(firstDigit)) {
		// if the firstdigit of the rasti is a number it gets parsed to an integer and sent back
		return parseInt(firstDigit);
	} else {
		// if it was not an integer a 0 is returned since it is a neutral value
		return 0;
	}
}

/*###########################################
 Pure utility functions to make coding easier
 ###########################################*/

// if the number is less than 0 it adds a leading zero. EX: 1 --> 01
function insertLeadingZero(number) {
	// if number is less than ten a leading zero is added
	// ex 8 --> 08
	if(number < 10) {
		// casted the integer to string
		var numberStr = number.toString();
		// 8 --> 08
		numberStr = "0" + numberStr;
		return numberStr;
	}
	//if there was nothing to do the actual number is returned as a string
	return number.toString();
}

// tests if a value that is passed in the parameters is in the array
function isValueInArray(value, array) {
	// starts the loop of the array passed
	for (let i in array) {
		// if the value is in the array then true is returned
		// values are casted in to string so that the conditions are equal
		if(array[i].toString() == value.toString()) {
			// value was found from the array
			return true;
		}
	}
	// value was not found from the array
	return false;
}

// checks is the number a whole number or not
// in a different function, to keep code more readable.
function isWholeNumber(num) {
	// test if the not a number is false and is decimal number
	if (!isNaN(num) && num % 1 == 0) {
		// if the number indeed is a number and is not a decimal number true is returned
    	return true;
	} 
	// if the number is something else then false is returned
	return false;
}

/*###########################################
THE VT1 Answers end here and VT2 answers start
############################################*/ 

// orders the team by sarja, then score and then name
function compareTeamsAndSeriesAndPoints(a,b) {
	const sarjaA = a.sarja;
	const sarjaB = b.sarja;
	const nameA = a.name;
	const nameB = b.name;
	const scoreA = a.score;
	const scoreB = b.score;
	let comparison = 0;
	if(sarjaA < sarjaB) {
		comparison = -1;
	} else if(sarjaA > sarjaB) {
		comparison = 1; 
	} else if(sarjaA === sarjaB) {
		if(scoreA < scoreB) {
		comparison = 1;
		} else if(scoreA > scoreB) {
		comparison = -1; 
		}
		else if(scoreA === scoreB) {
			if(nameA < nameB) {
				comparison = -1;
			} else if(nameA > nameB) {
				comparison = 1; 
			}
		}
	}
	return comparison;
}

// team name order functions desenfing
function orderNameDesc(a,b) {
	const nameA = a.name;
	const nameB = b.name;
	let comparison = 0;
	if(nameA < nameB) {
		comparison = 1;
	} else if(nameA > nameB) {
		comparison = -1; 
	}
	return comparison;
}

// team name order functions assending
function orderNameAsc(a,b) {
	const nameA = a.name;
	const nameB = b.name;
	let comparison = 0;
	if(nameA < nameB) {
		comparison = -1;
	} else if(nameA > nameB) {
		comparison = 1; 
	}
	return comparison;
}

// team name order functions 
function orderScoreDesc(a,b) {
	const nameA = a.score;
	const nameB = b.score;
	let comparison = 0;
	if(nameA < nameB) {
		comparison = -1;
	} else if(nameA > nameB) {
		comparison = 1; 
	}
	return comparison;
}

// team name order functions assending
function orderScoreAsc(a,b) {
	const nameA = a.score;
	const nameB = b.score;
	let comparison = 0;
	if(nameA < nameB) {
		comparison = 1;
	} else if(nameA > nameB) {
		comparison = -1; 
	}
	return comparison;
}

// team name order functions desending
function orderMatkaDesc(a,b) {
	const nameA = a.distance;
	const nameB = b.distance;
	let comparison = 0;
	if(nameA < nameB) {
		comparison = -1;
	} else if(nameA > nameB) {
		comparison = 1; 
	}
	return comparison;
}

// orders by distance asc
function orderMatkaAsc(a,b) { 
	const nameA = a.distance;
	const nameB = b.distance;
	let comparison = 0;
	if(nameA < nameB) {
		comparison = 1;
	} else if(nameA > nameB) {
		comparison = -1; 
	}
	return comparison;
}

// orders by time desc
function orderAikaDesc(a,b) {
	const nameA = a.time;
	const nameB = b.time;
	let comparison = 0;
	if(nameA < nameB) {
		comparison = -1;
	} else if(nameA > nameB) {
		comparison = 1; 
	}
	return comparison;
}

// orders by time asc
function orderAikaAsc(a,b) {
	const nameA = a.time;
	const nameB = b.time;
	let comparison = 0;
	if(nameA < nameB) {
		comparison = 1;
	} else if(nameA > nameB) {
		comparison = -1; 
	}
	return comparison;
}

// when this is called it desides what order the tables will be in the list
function sortInformationTable(whatPart) {
	switch(whatPart.toLowerCase()) {
    case "sarja":
        listTeams();
        break;
    case "joukkue":
        listTeams("joukkue");
        break;
    case "pisteet":
       	listTeams("pisteet");
        break;
    case "matka":
        listTeams("matka");
        break
    case "aika":
        listTeams("aika");
        break;
    default:
        return;
}
}

/* ###########################################
Functions that make it possible to print dist
############################################*/ 

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
	var R = 6371; // Radius of the earth in km
	var dLat = deg2rad(lat2-lat1);  // deg2rad below
	var dLon = deg2rad(lon2-lon1); 
	var a = 
		Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
		Math.sin(dLon/2) * Math.sin(dLon/2)
		; 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d = R * c; // Distance in km
	return d;
}

function deg2rad(deg) {
	return deg * (Math.PI/180)
}

/* ###########################################
Functions that make it possible to print stuff
############################################*/ 

function listTeams(order) {
	// creates the variable where the element tupa is fetched 
	const tupa = document.getElementById('tupa');
	emptyElement(tupa);
	// creates the table where the data is served
	const table = document.createElement("table");
	// creates the caption element for table
	const caption = document.createElement("caption");
	// inserts caption
	caption.textContent = "Tulokset";
	// inserts the caption to the table
	table.appendChild(caption);
	// headings that are used during the loop
	const headings = ["Sarja","Joukkue","Pisteet","Matka","Aika"];
	// creates the table row for headinhs
	const headingTr = document.createElement("tr");
	// loop to make code more readable and shorer
	for (let i in headings) {
		// loop inserts all the headings from the table 
		let tableHeading = document.createElement("th");
		// adds the link so that it is possible to get sort information
		const headingLink = document.createElement("a");
		// makes the link not to travel to another page
		headingLink.href = "#sort" + headings[i];
		// adds id to make sorting possible
		// WARNING: if this is modified the sorting will not work properly
		headingLink.id = "sort" + headings[i];
		headingLink.textContent = headings[i];
		tableHeading.appendChild(headingLink);
		headingTr.appendChild(tableHeading);
		// adds the ecent listener for the sorting
		headingLink.addEventListener("click", function (e) {
            e.preventDefault();
            // calls the sort function
            sortInformationTable(headings[i]);
            });
	}
	// gets info about the teams in the right order
	const teamInformation = getCompetitionInformationByTeam();
	// sorts out the teaminformation with a custom sort function

	// switch works in way that it asks what is the current order and 
	// desides after that in what order the function will be called,
	switch(order) {
	    case "sarja":
	    	// this is actually the default way to get stuff ordered
	       	teamInformation.sort(compareTeamsAndSeriesAndPoints);
	        currentorder = "default";
	        break;
       	// orders by team name
	    case "joukkue":
	    	if(currentorder != "joukkueDesc") {
	    		teamInformation.sort(orderNameAsc);
	    		currentorder = "joukkueDesc";
	    	} else {
	    		teamInformation.sort(orderNameDesc);
	        	currentorder = "joukkueAsc";
	    	}
	        break;
	    // orders by score
	    case "pisteet":
	       if(currentorder != "scoreDesc") {
	    		teamInformation.sort(orderScoreAsc);
	    		currentorder = "scoreDesc";
	    	} else {
	    		teamInformation.sort(orderScoreDesc);
	        	
	        	currentorder = "scoreAsc";
	    	}
	        break;
	    // orders by distance
	    case "matka":
	        if(currentorder != "matkaDesc") {
	    		teamInformation.sort(orderMatkaAsc);
	    		
	    		currentorder = "matkaDesc";
	    	} else {
	    		teamInformation.sort(orderMatkaDesc);
	        	currentorder = "matkaAsc";
	    	}
	        break;
        // orders by time used
	    case "aika":
	        if(currentorder != "aikaDesc") {
	    		teamInformation.sort(orderAikaAsc);
	    		currentorder = "aikaDesc";
	    	} else {
	    		teamInformation.sort(orderAikaDesc);
	        	
	        	currentorder = "aikaAsc";
	    	}
	        break;
	    // calls the default way
	    default:
	        teamInformation.sort(compareTeamsAndSeriesAndPoints);
	        currentorder = "default";
	}
	// adds the heading table row
	table.appendChild(headingTr);
	// starts the loop that adds all the elements to the html page
	for (let i in teamInformation) {
		// build the related table row
		const tr = document.createElement("tr");
		// builds the series table data
		const series = document.createElement("td");
		// builds the teamname table data
		const teamnametd = document.createElement("td");
		// builds the link in which the teamname will be stored
		const teamnamelink = document.createElement("a");
		// creates a parameter for the link that at the momemnt lists #joukkue
		teamnamelink.href = "#joukkue";
		//ads team id field
		teamnamelink.id = teamInformation[i]["id"];
		// team link click event handeler
		teamnamelink.addEventListener("click", 
        function (e) {
            e.preventDefault();
            // if the team name is clicked the function will make it possible for teams to be edited
            editTeamInDataStructure(teamInformation[i]["id"]);
            // builds the rasti modification form
            buildRastiModificationForm(teamInformation[i]);
            }
        , 
        false);
		// creates the linebreak which is required
		const br = document.createElement("br");
		// gets the team members from the data set
		const members = document.createTextNode(teamInformation[i]["members"].join(', '));
		// creates the points section 
		const points = document.createElement("td");
		// creates the distance section 
		const distance = document.createElement("td");
		// creates the time section 
		const time = document.createElement("td");

		// adds the text content for the series name
		series.textContent = teamInformation[i]["sarja"];
		// adds the link text for the teamname link
		teamnamelink.textContent = teamInformation[i]["name"];
		// adds the score to the points textcontent
		points.textContent = teamInformation[i]["score"];
		// adds the distance as textcontent
		distance.textContent = teamInformation[i]["distance"] + "km";
		// adds the time as textcontent
		time.textContent = teamInformation[i]["time"];
		// rest is quite self explanetory
		teamnametd.appendChild(teamnamelink);
		tr.appendChild(series);
		tr.appendChild(teamnametd);
		teamnametd.appendChild(br);
		teamnametd.appendChild(members);
		tr.appendChild(points);
		tr.appendChild(distance);
		tr.appendChild(time);
		table.appendChild(tr);
	}	
	// adds the table to the site
	tupa.appendChild(table);
	// is the actual event listener for the button
	function klikkikasittelija(e) {
		// this is here to prevent reload of the page
		e.preventDefault();

	}
}

// builds the form to add a rasti to the dataset
function buildFormForRastit() {
	// fetches the FIRST form from the page
    const form = document.querySelector("form");
    // builds the fieldset to match the requirements
    const fieldset = document.createElement("fieldset");
    form.appendChild(fieldset);

    // builds the legend
    const legend = document.createElement("legend");
    fieldset.appendChild(legend);
    fieldset.textContent = "Rastin tiedot";
    // this here is a table that adds the inputs in a loop
    const formInformation = ["Lat","Lon","Koodi"];
    // builds the table where the information is added
    for (let i in formInformation) {
    	// builds the paragraph element --> <p><label>Lat <input type="text" value="" /></label></p>
    	const p = document.createElement("p");
    	// builds the label
    	const lable = document.createElement("label");
    	// builds the input 
    	const input = document.createElement("input");
    	// adds th text to the label
  	    lable.textContent = formInformation[i];
  	    // appends the lable to the parrent element
	    p.appendChild(lable);
	    // next lines will define the parameters for the variables
	    input.type = "text";
	    // adds the so that it is possible to locate values precisly
	    input.id = formInformation[i];
	    // inputs value, this is used for debbuging
	 	//input.value = value[i];
	 	// appends lable and fieldset
	    lable.appendChild(input);
	    fieldset.appendChild(p);
	}
	// builds the button for the stuff to be uploaded
	const p = document.createElement("p");
	// creates the button element and adds text to it and appends it to paragraph element
	const button = document.createElement("button");
    button.textContent = "Lisää rasti";
    p.appendChild(button)
    // determines name and id to for the  button
    button.id = "rasti";
    button.name = "rasti";
    fieldset.appendChild(p);
    // adds a event listener for the button 
    button.addEventListener("click", klikkikasittelija);
	
	// is the actual event listener for the button
	function klikkikasittelija(e) {
		// this is here to prevent reload of the page
		e.preventDefault();
		// this is the rasti id that is checked to be one that is not use
		const rid = getNewRastiId();
		// gets the competetion id
		const kilpailu = data.id;
		// gets the latitude, longitude and the code from the form
		const lat = document.getElementById("Lat").value;
		const lon = document.getElementById("Lon").value;
		const koodi = document.getElementById("Koodi").value;
		// checks if all the values are present
		if((lat.length) && (lon.length) && (koodi)) {
			// creates a data set to be added in to the rastidataset
			const values = {
				kilpailu: kilpailu,
				id: rid,
				koodi: koodi,
				lat: lat,
				lon: lon,
				// gets the current time in a right form
				time: getCurrentTime()
					   
			};
			// appends at the end of the dataset
			data.rastit.push(values);
			document.getElementById("Lat").value = "";
			document.getElementById("Lon").value = "";
			document.getElementById("Koodi").value = "";
			// prints all the rastit to the console as requested
			for (let i in data.rastit) {
				console.log("Koodi: " + data.rastit[i].koodi + "; Lat: " + data.rastit[i].lat + "; Lon: " + data.rastit[i].lon);
			}
		}
		
	}
}

function buildFormForTeam() {
        // fetches the FIRST form from the page
	    const allForms = document.querySelectorAll("form");
	    // gets the second form
	    const form = allForms[1];
	    // empty the form
	    emptyElement(form);

	    // builds the fieldset to match the requirements
	    const fieldset = document.createElement("fieldset");
	    form.appendChild(fieldset);
	    const legend = document.createElement("legend");
	    fieldset.appendChild(legend);
	    fieldset.textContent = "Lisää joukkue";
	    const formInformation = ["Nimi","Jäsen","Jäsen"];
	    // builds the table where the information is added
    	// adds team name field
    	const p = document.createElement("p");
    	const lable = document.createElement("label");
  	    lable.textContent = "Joukkueen nimi";
	    p.appendChild(lable);

	    // adding input for team
	    const input = document.createElement("input");
	    input.type = "text";
	    // adds the so that it is possible to locate values precisly
	    input.id = "teamname";
	    // inputs value, this is used for debbuging
	 	input.value = "";
	 	// adds the eventlistener that makes sure that all the fields have information 
	 	// teamname and atleast twoteam members must be added
 	    input.addEventListener("input", function() { 
 	    	// gets the team members by using a custom funtion
	    	const members = getTeamMembersFromForm();
	    	// gets the team name from the form
	    	const teamname = document.getElementById("teamname").value;
	    	// adds a button
	    	const button = document.getElementById("teambutton");
	    	// makes sure that the info is in place
	    	if(members.length > 1 && teamname.length > 0) {
	    		button.disabled = false;
	    	} else {
	    		button.disabled = true;
	    	}
		} )
 	   	// appends input in a lable and the lable in to a paragraph
	    lable.appendChild(input);
	    fieldset.appendChild(p);

	    // adding team member fields
	    // spawning a div that makes it possible to append all in the same part of the form
	    const membersdiv = document.createElement("div");
	    membersdiv.id = "teammembersdiv";
	    fieldset.appendChild(membersdiv);
	    // adding the team field
	    addJasenInputField(membersdiv,"",false);
	    addJasenInputField(membersdiv,"",true);
	    const teamidfield = document.createElement("input");
	    // adds team name in to a hidden field
		teamidfield.setAttribute("type", "hidden");
		teamidfield.id = "teamidtid";
		teamidfield.value = "";
		membersdiv.appendChild(teamidfield);
	    // builds the button for the stuff to be uploaded
	    addButtonToTeamForm(fieldset);
}

function buildRastiModificationForm(teamInformation) {
	 // fetches the all the forms from the page
    const allForms = document.querySelectorAll("form");
    // gets the second form that is the one we are going to use
    const form = allForms[1];
    // creates and appends fieldset
    const fieldset = document.createElement("fieldset");
    form.appendChild(fieldset);
    // creates and appends a legend
    const legend = document.createElement("legend");
    fieldset.appendChild(legend);
    fieldset.textContent = "Rastit";
    // creates a paragraph label and then adds the text
    var p = document.createElement("p");
    var lable = document.createElement("label");
    lable.textContent = "Muokkaa olemassa olevaa rastia ";
    // lable is added to the paragraph
	p.appendChild(lable);
    fieldset.appendChild(p);
    // builds the dropdown menu
    const rastiDropDown = document.createElement("select");
    // gives the dropdown a id
    rastiDropDown.id = "rastiDropDown";
    p.appendChild(rastiDropDown);
    // builds the first option in to the dropdown
    var option = document.createElement("option");
    // this is the zero option, with this we know that there was no rasti chosen
    option.value = "0";
    // Human readable text to correspond to the zero
    option.textContent = "Valitse Rasti";
    rastiDropDown.appendChild(option);
    // builds the fieldset to match the requirements
    // is done in loops to keep code shorter and readable
    const formInformation = ["Koodi","Time","rastiId","joukkueId","tupaindex"];
    const placeholders = ["","yyyy-mm-dd hh:mm:ss"];
    // builds the table where the information is added
    for (let i in formInformation) {
    	// builds the paragraph element --> <p><label>Lat <input type="text" value="" /></label></p>
    	const p = document.createElement("p");
    	// builds the label
    	const lable = document.createElement("label");
    	// builds the input 
    	const input = document.createElement("input");
    	// adds th text to the label
  	    lable.textContent = formInformation[i];
  	    // appends the lable to the parrent element
	    p.appendChild(lable);
	    // next lines will define the parameters for the variables
	    input.type = "text";
	    // adds the so that it is possible to locate values precisly
	    input.id = "rmod" + formInformation[i];
	    // if there is a placeholder it gets add here
	    if(placeholders[i]) {
	    	input.placeholder = placeholders[i];
	    }
	    // for he hidden ones that start after the second these options are added.
	    if(i>1) {
			input.setAttribute("type", "hidden");
			p.hidden = true;
			lable.hidden = true;
			input.setAttribute("id", formInformation[i]);
			input.setAttribute("value", "");
	    }
	 	// appends lable and fieldset
	    lable.appendChild(input);
	    fieldset.appendChild(p);
	}
	// adds the rastit of a spesific team to the options
	for (let i in teamInformation.rastit) {
		// creates the option and then gets info from the datastructure
    	const option = document.createElement("option");
	    option.value = teamInformation.rastit[i].koodi;
	    // adds human readable text
	    option.textContent = "Rasti " + i + "; " + teamInformation.rastit[i].koodi;
	    //console.log(teamInformation.rastit[i].koodi)
	    rastiDropDown.appendChild(option);
	    // adds the event handeler to the options
	    option.addEventListener("click", klikkikasittelija);
	    // actual event handeler
	    function klikkikasittelija(e) {
			// this is here to prevent reload of the page
			e.preventDefault();
			// These informations are visible on the page
			document.getElementById("rmodKoodi").value = teamInformation.rastit[i].koodi;
			document.getElementById("rmodTime").value = teamInformation.rastit[i].time;
			// this indormation is hidden in the page
			document.getElementById("joukkueId").value = teamInformation.id;
			document.getElementById("rastiId").value = teamInformation.rastit[i].id;
			document.getElementById("tupaindex").value = teamInformation.rastit[i].index;
		}
    }
    // buttons paragraph to hold the putton in place
	const buttonP = document.createElement("p");
	// creates the button element and adds text to it and appends it to paragraph element
	const addButton = document.createElement("button");
    addButton.textContent = "Muokkaa rastia";
    buttonP.appendChild(addButton)
    // determines name and id to for the  button
    addButton.id = "rmodAddButton";
    addButton.name = "team";
    addButton.disabled = false;
    fieldset.appendChild(buttonP);
    // adds a event listener for the button 
    addButton.addEventListener("click", muokkaaKlikkikasittelija);
    	function muokkaaKlikkikasittelija(e) {
		// this is here to prevent reload of the page
		e.preventDefault();
		// edist the rasti chosen
		editRasti(teamInformation.rastit[i].index);
		// adds a new team to the structure
		addTeamToDataStruture();
		// updates the teams on the page
		updateTeams();
		// emptys the team form
		emptyTeamForm();
		// builds a new 
		buildFormForTeam();
	}
	// creates the remove button
    const removeButton = document.createElement("button");
    removeButton.textContent = "Poista rasti";
    buttonP.appendChild(removeButton);
    // determines name and id to for the  button
    removeButton.id = "rmodAddButton";
    removeButton.name = "team";
    // not disabled
    removeButton.disabled = false;
    fieldset.appendChild(buttonP);
    // adds a event listener for the button 
    removeButton.addEventListener("click", poistoKlikkikasittelija);
    // is the actual event listener for the button
	function poistoKlikkikasittelija(e) {
		// this is here to prevent reload of the page
		e.preventDefault();
		removeRasti();
		updateTeams();
		emptyTeamForm();
		buildFormForTeam();
		listTeams();
	}

}
// removes the rasti from tupa
// does not work...
function removeRasti() {
	const tupaindex = document.getElementById("tupaindex").value;
	// nice try, but time is up
	delete data.tupa[tupaindex];

}

// edits the rasti, does not work
function editRasti(index) {
	data.tupa[index].aika = "";

}
// adds the jasen field when it is needed
function addJasenInputField(fieldset,value,eventhandeling) {
	// builds the paragraph element --> <p><label>Lat <input type="text" value="" /></label></p>
	const p = document.createElement("p");
	// builds the label
	const lable = document.createElement("label");
	// builds the input 
	const input = document.createElement("input");
	// adds th text to the label
    lable.textContent = "Jäsen";
    // appends the lable to the parrent element
    p.appendChild(lable);
    // next lines will define the parameters for the variables
    input.type = "text";
    // adds the so that it is possible to locate values precisly
    input.className = "jasenfield";
    // inputs value, this is used for debbuging
 	input.value = value;
 	// appends lable and fieldset
    lable.appendChild(input);
    fieldset.appendChild(p);
    // adds the event listener
    input.addEventListener("input", function() { 
    	// gets the member info
    	const members = getTeamMembersFromForm();
    	// gets the button
    	const button = document.getElementById("teambutton");
    	// gets the team name info
    	const teamname = document.getElementById("teamname").value;
    	// if there are atleast two members and the name is filled the button gets enabled
    	if(members.length > 1 && teamname.length > 0) {
    		button.disabled = false;
    	} else {
    		button.disabled = true;
    	}


    } )
    // if the eventhandeling is true then the last form appends one field when needed
    if(eventhandeling) {
    	// gets all the forms
 		const allOtherForms = document.querySelectorAll(".jasenfield");
 		// adds input
 		input.addEventListener("input", function() { addJasenInputField(fieldset,"",true) }, { once: true } );
 	}
}

// edits team information in the data structure
function editTeamInDataStructure(teamid) {
	// indexs so that he info can be used later
	var indexi = 0;
	var indexj = 0;
	// loops through the sarjat
	for (let i in data.sarjat) {
		// loops through the teams in the sarjat
		for (let j in data.sarjat[i].joukkueet) {
			// stores the team id
			let joukkueid = data.sarjat[i].joukkueet[j].id;
			// checks if the rast id is in use. If it is not then id is returned at the end
			if(teamid === joukkueid) {
				// if id is in use then found is se to 1 and the loop gets a new id
				buildFormForTeam();
				// adds the team nam ein to the form
				document.getElementById("teamname").value = data.sarjat[i].joukkueet[j]["nimi"];
				// emties the members
				emptyElement(document.getElementById("teammembersdiv"));
				// adds the members in to the div
				for (let k in data.sarjat[i].joukkueet[j].jasenet) {
					addJasenInputField(document.getElementById("teammembersdiv"),data.sarjat[i].joukkueet[j].jasenet[k],false);
				}
				// gets the indexes for later use
				indexi = i;
				indexj = j;
				// since the team is fond we can quit the loop
				break;
			}
		}
	}
	// adds an empty field after the last member
	addJasenInputField(document.getElementById("teammembersdiv"),"",true);
	// empties the button
	emptyElement(document.getElementById("teambutton"));
	// gets all the fieldsets
	const fieldsets = document.querySelectorAll("fieldset");
	// gets the fildset that we need
	const fieldset = fieldsets[1];
	// removes the button
	fieldset.removeChild(fieldset.lastChild);
	// creates the button paragraph
	const buttonP = document.createElement("p");
	// creates the button element and adds text to it and appends it to paragraph element
	const button = document.createElement("button");
	// since we have clicked a team we are going to edit it and the text in the button will correspond to that
    button.textContent = "Muokkaa joukkuetta";
    buttonP.appendChild(button);
    // determines name and id to for the  button
    button.id = "teambutton";
    button.name = "team";
    // button has to be disabled until the textboxes have been filled
    button.disabled = true;
    fieldset.appendChild(buttonP);
    // adds a event listener for the button 
    button.addEventListener("click", klikkikasittelija);
    // is the actual event listener for the button
	function klikkikasittelija(e) {
		// this is here to prevent reload of the page
		e.preventDefault();
		editTeamInData(indexi,indexj);
		updateTeams();
		buildFormForTeam();
	}
}

// edits teams in the data structure
function editTeamInData(indexi,indexj) {
	data.sarjat[indexi].joukkueet[indexj].jasenet = getTeamMembersFromForm();
	data.sarjat[indexi].joukkueet[indexj].nimi = document.getElementById("teamname").value;
}

// gets members from the form
function getTeamMembersFromForm() {
	// gets the element
	const jasenfields = document.querySelectorAll(".jasenfield");
	var members = [];
	for (let i = 0; i < jasenfields.length; i++) {
		// gets the value
		let member = jasenfields[i].value;
		// trims the whitespace away
		member = member.trim();
		// if the field is not empty then the member is pushed
		if(member.length > 0) {
			members.push(member)
		}
	}
	return members;
}

// adds team to the datastructure when it is new
function addTeamToDataStruture() {
	var team = [];
	// gets the team name
	const teamname = document.getElementById("teamname").value;
	// gets a new unique team id
	var teamid = getNewTeamId();
	// builds a data structure
	team.push({
		id: teamid,
		// stores the team name in the key name
	    jasenet:   getTeamMembersFromForm(),
	    // stores the sarja of team
	    last: getCurrentTime(), 
		// name of team
		nimi: teamname
	});
	
	// This tests that all he information is set
	if(teamname.length > 0 && team[0]["jasenet"].join().length > 1) {
			// sarja 1 means 2h
			data.sarjat[1].joukkueet.push(team[0]);
		}
	// updates the teams
	updateTeams();
	
}

// adds a button to the team form that adds a new team
function addButtonToTeamForm(fieldset) {
	const buttonP = document.createElement("p");
	// creates the button element and adds text to it and appends it to paragraph element
	const button = document.createElement("button");
    button.textContent = "Lisää joukkue";
    buttonP.appendChild(button);
    // determines name and id to for the  button
    button.id = "teambutton";
    button.name = "team";
    button.disabled = true;
    fieldset.appendChild(buttonP);
    // adds a event listener for the button 
    button.addEventListener("click", klikkikasittelija);
    // is the actual event listener for the button
	function klikkikasittelija(e) {
		// this is here to prevent reload of the page
		e.preventDefault();
		addTeamToDataStruture()
		emptyTeamForm();
		buildFormForTeam();
	}
}



// creates a new rasti id and checks that it is not in use already!
function getNewRastiId() {
	while(true) {
		var found = 0;
		// creates the id
		const id = Math.floor(Math.random() * 7000000000000000) + 4000000000000000; 
		for (let i in data.rastit) {
			let rid = data.rastit[i].id;
			// checks if the rast id is in use. If it is not then id is returned at the end
			if(id === rid) {
				// if id is in use then found is se to 1 and the loop gets a new id
				found = 1;
				break;
			}
		}
		if (found == 0) {
			return id;
		}
	}
}

function getNewTeamId() {
	while(true) {
		var found = 0;
		// creates the id
		const id = Math.floor(Math.random() * 7000000000000000) + 4000000000000000; 
		for (let i in data.sarjat) {
			for (let j in data.sarjat[i].joukkueet) {
				let joukkueid = data.sarjat[i].joukkueet[j].id;
				// checks if the rast id is in use. If it is not then id is returned at the end
				if(id === joukkueid) {
					// if id is in use then found is se to 1 and the loop gets a new id
					found = 1;
					break;
				}
			}
		}
		if (found == 0) {
			return id;
		}
	}
}

// Builds a date for the rasti datastructure
function getCurrentTime() {
	// gets the time right now
	let currentDate = new Date();
	// formats the time to form EX: "2017-03-18 16:04:27"
	let strCurrentDate = currentDate.getFullYear() + "-" + insertLeadingZero(currentDate.getMonth()+1) + "-" + insertLeadingZero(currentDate.getDate()) + " " + insertLeadingZero(currentDate.getHours()) + ":" + insertLeadingZero(currentDate.getMinutes()) + ":" + insertLeadingZero(currentDate.getSeconds());
	return strCurrentDate;
}

// if the number is less than 0 it adds a leading zero. EX: 1 --> 01
function insertLeadingZero(number) {
	// if number is less than ten a leading zero is added
	// ex 8 --> 08
	if(number < 10) {
		// casted the integer to string
		var numberStr = number.toString();
		// 8 --> 08
		numberStr = "0" + numberStr;
		return numberStr;
	}
	//if there was nothing to do the actual number is returned as a string
	return number.toString();
}

/* ###########################################
Update, emptying and modification functions
############################################*/ 

// updates the teams in the page
function updateTeams() {
	// updates the page and the team sctructure
	// creates the variable where the element tupa is fetched 
	const tupa = document.getElementById('tupa');
	// clears the element so that if there is content it gets wiped
	emptyTeamForm(tupa);
	listTeams();
}

// empties the team form
function emptyTeamForm() {
	document.getElementById("teamname").value = "";
	const jasenfields = document.querySelectorAll(".jasenfield");
	var members = [];
	for (let i = 0; i < jasenfields.length; i++) {
		jasenfields[i].value = "";
	}
}

// empties any elemet
function emptyElement(element) {
	while(element.firstChild){
    	element.removeChild(element.firstChild);
	}
}

/* ###########################################
This is where actually stuff gets executed
############################################*/ 

window.onload = function () {
	// lists team on the page
	listTeams();
	// builds rasti form
	buildFormForRastit();
	// builds a team form
	buildFormForTeam();
}